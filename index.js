/* *
 * This sample demonstrates handling intents from an Alexa skill using the Alexa Skills Kit SDK (v2).
 * Please visit https://alexa.design/cookbook for additional examples on implementing slots, dialog management,
 * session persistence, api calls, and more.
 * */
const Alexa = require('ask-sdk-core');
const i18n = require('i18next');
const sprintf = require('i18next-sprintf-postprocessor');
const Parser = require('rss-parser');
const fs = require('fs');
const moment = require('moment-timezone');
var miRecordatorio, opcion;
var persistenceAdapter = getPersistenceAdapter();
const skillBuilder = Alexa.SkillBuilders.custom().withPersistenceAdapter(persistenceAdapter);
const GIVEN_NAME_PERMISSION = ['alexa::profile:given_name:read'];
// We create a language strings object containing all of our strings. 
// The keys for each string will then be referenced in our code
// e.g. requestAttributes.t('WELCOME_MSG')
const languageStrings =
{
    es:
    {
        translation:
        {
            Bienvenida_MSG:
            {
                MSG_0:
                    [
                        "Bienvenido %s, me encargaré de recordarte cuando tomar los medicamentos.",
                        "%s espero que estés pasando un muy buen día,",
                        "¿Dime %s que te gustaría hacer hoy?",
                        "Bienvenido %s, soy Medi-reco, estoy aquí para acompañarte en todo momento."
                    ],
                MSG_1:
                    [
                        "conmigo, ejercitarás tu memoria para que no olvides nada. ",
                        "junto a mi, seguirás en tratamiento médico correctamente. ",
                        "a mi lado, tu tratamiento médico será un éxito. "
                    ],
                MSG_2:
                    [
                        " ",
                        " si lo deseas, podrás programar o dar un repaso por los horarios de los medicamentos que debes tomar;",
                        " cuando quieras, puedes agregar, cambiar o eliminar medicamentos;",
                        " ",
                        " en cualquier momento, puedes agregar nuevos medicamentos para ser recordados cuando sea necesario;",
                        " ",
                        " si tu quieres, puedes modificar o eliminar los medicamentos que ya tengas registrados;"
                    ],
                MSG_3:
                    [
                        " y si gustas, te Puedo contar noticias con la actualidad de lo que está pasando el día de hoy.",
                        " Tambien Puedo contarte las noticias que están pasando hoy en colombia.",
                        " Tambien te informaré con noticias de lo que está pasando hoy."
                    ],
                MSG_4:
                    [
                        " Bueno, podemos hablar cuando lo desees.",
                        " Sólo dime lo que quieres y comencemos a platicar.",
                        " Dime lo que te gustaría hacer, estamos en confianza."
                    ]
            },
            Noticia_MSG:
            {
                MSG_1:
                    [
                        "Hola %s, ¿Qué te parece si te enteras de una nueva noticia?",
                        "Hola %s, ¿Cómo estás? ¿Qué te parece si te cuento una nueva noticia?"
                    ]
                ,
                MSG_2:
                    [
                        " ¿Quieres una noticia al azar o prefieres una noticia de de un tema en particular?",
                        " ¿Quieres una noticia al azar o prefieres una  noticia de algun tema?",
                        " ¿Quieres una noticia al azar o prefieres una noticia de un tema en especifico?",
                        " ¿Quieres una noticia al azar o que tipo de noticia quieres oir?",
                        " ¿Quieres oír una noticia en general, o de deportes, política, judicial, ciencia o alguna otra sección?"
                    ],
                MSG_3:
                    [
                        " O si lo deseas, podemos hablar más tarde.",
                        " Dime lo que te gustaría hacer, estamos en confianza.",
                        " Dime lo que te gustaría oír, estamos en confianza."
                    ]
            },
            MSG_INVITACION:
                [
                    "Quieres saber más noticias como esta, solo dí: abre mis medicamentos diarios y pideme una noticia.",
                    "Puedo contarte más noticias, dí: abre mis medicamentos diarios y pideme que te cuente una noticia.",
                    "Esta y otras noticias nuevas las podrás conocer si dices: inicia mis medicamentos diarios para que empecemos a hablar."
                ],
            Noticia_URL:
                [
                    "https://www.eltiempo.com/rss/politica.xml",//politica
                    "https://www.eltiempo.com/rss/economia.xml",//
                    "https://www.eltiempo.com/rss/tecnosfera.xml",//tecnología
                    "https://www.eltiempo.com/rss/cultura.xml",//Cultura
                    "https://www.eltiempo.com/rss/justicia.xml",//judicial
                    "https://www.eltiempo.com/rss/deportes.xml",//Deportes
                    "https://www.eltiempo.com/rss/colombia_otras-ciudades.xml",//ciudades
                    "https://www.eltiempo.com/rss/mundo.xml",//internacional
                    "https://www.eltiempo.com/rss/deportes_futbol-colombiano.xml",//Fútbol colombiano
                    "https://www.eltiempo.com/rss/opinion.xml",//Opinion
                    "https://www.eltiempo.com/rss/vida_salud.xml",//salud
                ],
            Noticia_politica:
                [
                    "https://www.eltiempo.com/rss/politica.xml",//politica
                ],
            Noticia_economia:
                [
                    "https://www.eltiempo.com/rss/economia.xml",//
                ],
            Noticia_tecno:
                [
                    "https://www.eltiempo.com/rss/tecnosfera.xml",//tecnología
                    "https://www.eltiempo.com/rss/tecnosfera_novedades-tecnologia.xml",
                    "https://www.eltiempo.com/rss/tecnosfera_tutoriales-tecnologia.xml",
                    "https://www.eltiempo.com/rss/tecnosfera_videojuegos.xml",
                ],
            Noticia_judicial:
                [
                    "https://www.eltiempo.com/rss/justicia.xml",//judicial
                ],
            Noticia_deportes:
                [
                    "https://www.eltiempo.com/rss/deportes.xml",//Deportes
                    "https://www.eltiempo.com/rss/deportes_futbol-colombiano.xml",//Fútbol colombiano
                ],
            Noticia_nacional:
                [
                    "https://www.eltiempo.com/rss/colombia_otras-ciudades.xml",//ciudades
                ],
            Noticia_internacional:
                [
                    "https://www.eltiempo.com/rss/mundo.xml",//internacional
                ],
            Noticia_farandula:
                [
                    "https://www.eltiempo.com/rss/opinion.xml",//Opinion
                    "https://www.eltiempo.com/rss/cultura.xml",//Cultura
                ],
            Noticia_salud:
                [
                    "https://www.eltiempo.com/rss/vida.xml",//salud
                    "https://www.eltiempo.com/rss/cultura_gente.xml",
                ],

            REGISTER_MSG:
            {
                MSG_1:
                    [
                        'He registrado el nuevo recordatorio con exito.',
                        'Recordatorio registradocon exito.',
                        'Recordatorio agendado en mi sistema.',
                        'Recordatorio agendado en mi sistema satisfactoriamente.'
                    ],
                MSG_2:
                    [
                        'Vale, recordare que tienes que tomar el medicamento de color %s para %s cada %s.',
                        'De ahora en adelante te avisaré en que hora tienes que tomar el medicamento %s para %s cada %s.',
                        'Te avisaré sin falta en que momento tienes que tomar la dosis del medicamento %s para %s cada %s'
                    ]
            },
            ACTUALIZAR_MSG:
            {
                MSG_1:
                    [
                        'He actualizado el  recordatorio con exito.',
                        'Recordatorio modificado con exito.',
                        'Recordatorio actualizado en mi sistema.',
                        'Recordatorio modificado en mi sistema satisfactoriamente.'
                    ],
                MSG_2:
                    [
                        'Vale, te recordaré por el medicamento para %s cada %s todos los días.',
                        'De ahora en adelante te avisaré en que hora tienes que tomar o aplicarte el medicamento para %s cada %s.',
                        'Te avisaré sin falta en que momento tienes recordar el medicamento para %s cada %s a diario'
                    ]
            },
            RECORDATORIO_MSG:
            {
                MEDICAMENTO_MSG:
                    [
                        '%s, ya tienes que tomar el medicamento para %s, recuerda, es %s, deberas tomar de nuevo en  %s.',
                        '%s, es hora de tomar el medicamento para  %s, recuerda que es %s, en %s debes tomarla nuevamente.',
                        '%s, no te olvides de tomar el medicamento para %s, recuerda que es %s, te recordaré en %s.',
                        'Hola %s, te quiero recordar que debes tomar la medicina para %s,  es %s, despues debes tomar en %s.',
                    ],
                SUEGERENCIA_MSG:
                    [
                        "Hola %s, tengo una nueva noticia, ¿Quieres oirla? solo dí inicia Medi-reco y pedime agregar un medicamento",
                        "Hola %s, estoy aquí acompañandote, solo dí inicia Medi-reco y registra o modifica un medicamento",
                        "Hola %s, ¿Quieres conocer algo que ha pasado en Nariño? solo dí inicia Medi-reco y pideme una nueva noticia",
                        "Que tal %s, ¿Hola quieres hablar conmigo? solo dí inicia Medi-reco y pideme que te cuente una noticia",
                        "Hola %s, tengo una nueva noticia, ¿Quieres oirla? solo dí inicia mis medicamentos diarios y pedime agregar un medicamento",
                        "Hola %s, estoy aquí acompañandote, solo dí inicia mis medicamentos diarios y registra o modifica un medicamento",
                        "Hola %s, ¿Quieres conocer algo que ha pasado en Nariño? solo dí abre mis medicamentos diarios y pideme una nueva noticia",
                        "Que tal %s, ¿Hola quieres hablar conmigo? solo dí abre mis medicamentos diarios y pideme que te cuente una noticia",
                    ],
                FORMATO_MSG: ' color %s que tomas a las %s cada %s',
            },
            HELP_SIMPLE_MSG: [
                'Haz pedido una guia rápida. cuando lo desees, puedes pedirme registrar, modificar o eliminar uno de los medicamentos, por ejemplo, ¡listar medicamentos!   También, cuando lo desees te contaré una noticia, puede ser de un tema en general o de uno en particular si así lo quieres. ¿Te ha quedado claro? Ahora intenta pedirme algo, por ejemplo: pídeme registrar un medicamento.',
                'Haz pedido una guia rápida. cuando lo desees, puedes pedirme registrar, modificar o eliminar uno de los medicamentos, por ejemplo, ¡Registrar medicamento! También, cuando lo desees te contaré una noticia, puede ser de un tema en general o de uno en particular si así lo quieres. ¿Te ha quedado claro? Ahora intenta pedirme algo, por ejemplo: me puedes pedir que liste los medicamentos.',
                'Haz pedido una guia rápida. cuando lo desees, puedes pedirme registrar, modificar o eliminar uno de los medicamentos, por ejemplo, ¡modificar medicamento! También, cuando lo desees te contaré una noticia, puede ser de un tema en general o de uno en particular si así lo quieres. ¿Te ha quedado claro? Ahora intenta pedirme algo, por ejemplo: solicítame eliminar un medicamento.',
                'Haz pedido una guia rápida. cuando lo desees, puedes pedirme registrar, modificar o eliminar uno de los medicamentos, por ejemplo, ¡borrar un medicamento! También, cuando lo desees te contaré una noticia, puede ser de un tema en general o de uno en particular si así lo quieres. ¿Te ha quedado claro? Ahora intenta pedirme algo, por ejemplo: puedes decir, modificar un medicamento registrado.',
                'Haz pedido una guia rápida. cuando lo desees, puedes pedirme registrar, modificar o eliminar uno de los medicamentos, por ejemplo, ¡editar medicamentos!   También, cuando lo desees te contaré una noticia, puede ser de un tema en general o de uno en particular si así lo quieres. ¿Te ha quedado claro? Ahora intenta pedirme algo, por ejemplo: dí algo cómo: dame una noticia nueva.',
                'Haz pedido una guia rápida. cuando lo desees, puedes pedirme registrar, modificar o eliminar uno de los medicamentos, por ejemplo, ¡Registrar medicamento! También, cuando lo desees te contaré una noticia, puede ser de un tema en general o de uno en particular si así lo quieres. ¿Te ha quedado claro? Ahora intenta pedirme algo, por ejemplo: pide algo cómo: dame una noticia de deportes.',
                'Haz pedido una guia rápida. cuando lo desees, puedes pedirme registrar, modificar o eliminar uno de los medicamentos, por ejemplo, ¡Agregar medicamento  ! También, cuando lo desees te contaré una noticia, puede ser de un tema en general o de uno en particular si así lo quieres. ¿Te ha quedado claro? Ahora intenta pedirme algo, por ejemplo: solciita algo cómo: dame una noticia de politica.',
            ],
            HELP_MSG: 'Me haz pedido asistencia, puedo darte una ayuda rápida, pero si quieres una guía completa, debes decir: necesito una guía completa. ¿Qué tipo de ayuda necesitas?',
            GOODBYE_MSG: 'Hasta luego!.',
            REFLECTOR_MSG: 'Acabas de activar %s. esta funcionalidad aún está en desarrollo, intenta con una frese distina',
            FALLBACK_MSG: 'Lo siento, no se nada sobre eso. Por favor inténtalo otra vez.',
            MSG: 'No hay recodatorios que mostrar, para agregar un nuevo medicamento, dime: ¡agregar nuevo medicamento!',
            ERROR_MSG: 'Ha ocurrido un problema. Asegurate de tener todos los permisos activados y vuelve a intentarlo.',
            CONTINUE_MSG: '¿haz dicho algo? no lo he entendido, si Quieres realizar algo más, Recuerda que puedes pedirme registrar, eliminar o modificar medicamentos, tambien podemos escuchar nuevas noticias o hablar un rato. Quedo pendiente',
            HELP_COMPLETE_MSG: 'Me haz pedido darte una guía completa, a continuación daremos un repaso por todas las funcionalidades que puedo hacer para mejorar tu tratamiento médico y mejorar tu memoria charlando sobre alguna nueva noticia. ¡Empecemos! Si es la primera vez que hablamos, no tengo ningún medicamento al cual recordarte cuando tomarlo, por tanto, para agregar un medicamento nuevo puedes decir frases como registrar, agregar un medicamento o guardar nuevo medicamento, después te pediré datos como un color con el cual identificarlo, la hora y la frecuencia de medicación. Si guardaste mal un medicamento, ¡no te preocupes! Puedes corregirlo con frases como: modificar, corregir medicamento o quiero editar un medicamento. Cuando hayas terminado un tratamiento o cuando desees que ya no te recuerde más por un medicamento, fácilmente me puedes decir frases cómo eliminar, borrar u olvidar medicamento. Cuando lo desees, puedes pedirme que recuerde todos los medicamentos que has registrado, con frases como listar medicamentos ó, cuéntame que medicamentos tengo guardados, y te daré una lista de los medicamentos que has guardado con sus colores y horarios. Y por último, en cualquier momento puedes pedirme que te cuente una noticia nueva, ó, que te cuente una noticia cualquiera, pero si deseas una noticia de un tema en específico por ejemplo, de deportes, política, salud, opinión, farándula, deportes, economía o internacional, puedes decirme: dame una noticia de, seguido del tema de tu interés. Debes tener en cuenta que estoy en la capacidad de recordar hasta seis medicamentos diferentes, y cada vez que quieras hablar conmigo debes decir: Alexa, abre mis medicamentos diarios. ¡Ahora es tu turno de hablar! cuéntame, ¿Que deseas hacer?'
        }
    }
}

let consultarNoticia = async (url) => {
    let parser = new Parser();
    let feed = await parser.parseURL(url);
    let noticia = feed.items[Math.floor(Math.random() * (feed.items.length))];
    return noticia;
}


function busqueda(recordatorios, description) {
    console.log("estoy en busqueda");
    let resultado = null;
    recordatorios.forEach((item, i) => {
        if (item.description === description) {
            resultado = item;
        }
    });
    return resultado;
}

function validarPermisos(handlerInput) {
    const { attributesManager, serviceClientFactory, requestEnvelope } = handlerInput;
    const { permissions } = requestEnvelope.context.System.user;

    if (permissions) {
        return true;
    }
    else return false;
}


const activarPermisosIntent = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'activarPermisosIntent';
    },
    async handle(handlerInput) {
        console.log("dentro de activar permisos intent");

        let mensaje = "dentro de activarPermisosIntent";
        return handlerInput.responseBuilder
            .addDirective({
                type: "Connections.SendRequest",
                name: "AskFor",
                payload: {
                    "@type": "AskForPermissionsConsentRequest",
                    "@version": "2",
                    "permissionScopes": [
                        {
                            "permissionScope": "alexa::alerts:reminders:skill:readwrite",
                            "consentLevel": "ACCOUNT"
                        }
                    ]
                },
                token: "token string"
            }).getResponse();
    }
};
//return activarPermisosIntent.handle(handlerInput);
const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return (
            Alexa.getRequestType(handlerInput.requestEnvelope) === "LaunchRequest"
        );
    },
    async handle(handlerInput) {
        const {
            attributesManager,
            serviceClientFactory,
            requestEnvelope,
            responseBuilder,
        } = handlerInput;
        const requestAttributes = attributesManager.getRequestAttributes();
        const sessionAttributes = attributesManager.getSessionAttributes();
        let parser = new Parser();

        try {
            const upsServiceClient = serviceClientFactory.getUpsServiceClient();
            const sessionAttributes = attributesManager.getSessionAttributes();
            const profileName = await upsServiceClient.getProfileGivenName();
            const name = await upsServiceClient.getProfileGivenName();
            const reminderApiClient =
                handlerInput.serviceClientFactory.getReminderManagementServiceClient();
            const mensajeDiario = sessionAttributes["mensajeDiario"]
                ? sessionAttributes["mensajeDiario"]
                : "";

            let speechText = "";
            let noticia = "";
            let aux = "";
            speechText = requestAttributes.t("RECORDATORIO_MSG.SUEGERENCIA_MSG", {
                returnObjects: true,
            });

            if (!sessionAttributes["recordatorio"]) {
                sessionAttributes["recordatorio"] = [];
            }

            speechText = requestAttributes.t("MSG_INVITACION", {
                returnObjects: true,
            });
            aux = speechText[Math.floor(Math.random() * speechText.length)];
            aux = aux.replace("%s", name);

            let speechNoticia = requestAttributes.t("Noticia_URL", {
                returnObjects: true,
            });
            let url =
                speechNoticia[Math.floor(Math.random() * speechNoticia.length - 1)];
            let ocurioError = "";
            await parser
                .parseURL(url)
                .then(async (noticiasRSS) => {
                    noticia +=
                        noticiasRSS.items[
                            Math.floor(Math.random() * noticiasRSS.items.length)
                        ].title;
                    noticia += ". " + speechText[Math.floor(Math.random() * speechText.length)];
                    noticia = noticia.replace("%s", name);
                    let horas = [
                        "FREQ=DAILY;BYHOUR=6;BYMINUTE=10;BYSECOND=0;INTERVAL=0;",
                    ];
                    let reminder = crearRecordarioUnico(noticia);
                    let alertToken = sessionAttributes["diario"];
                    console.log("mensaje2 " + noticia);

                    await reminderApiClient
                        .updateReminder(alertToken, reminder)
                        .catch((error) => {
                            reminderApiClient
                                .createReminder(reminder)
                                .then((reminderRequest) => {
                                    sessionAttributes["diario"] = reminderRequest.alertToken;
                                })
                                .catch((error) => {
                                    console.log(
                                        "Error en LaunchRequestHandler actualizar reminder: " +
                                        JSON.stringify(error)
                                    );
                                    const speechText = "MEdi-reco no tiene los permisos necesarios para el correcto funcionamiento, necesito los permisos de recordatorio para poderte informar cuando deberás tomar un medicamento. Por favor Vé a la pantalla principal de tu aplicación Alexa y concede los permisos requeridos";
                                    return responseBuilder
                                        .speak(speechText)
                                        .withAskForPermissionsConsentCard([
                                            "alexa::alerts:reminders:skill:readwrite",
                                        ])
                                        .withShouldEndSession(true)
                                        .getResponse();
                                });
                        });
                    console.log("LaunchRequestHandler4");
                    noticia +=
                        noticiasRSS.items[
                            Math.floor(Math.random() * noticiasRSS.items.length)
                        ].title;
                    noticia +=
                        ". " + speechText[Math.floor(Math.random() * speechText.length)];
                    noticia = noticia.replace("%s", name);
                    console.log("LaunchRequestHandler4.1");
                })
                .catch((error) => {
                    console.log(
                        "Error en LaunchRequestHandler leer noticia: " +
                        JSON.stringify(error)
                    );
                    return handlerInput.responseBuilder
                        .speak(
                            "Hubo un error al consultar una noticia. Asegurate de que tienes conexion a internet y vuelve a intentar "
                        )
                        .withShouldEndSession(true)
                        .getResponse();
                });
            let speechText0 = requestAttributes.t("Bienvenida_MSG.MSG_0", {
                returnObjects: true,
            });
            let speechText1 = requestAttributes.t("Bienvenida_MSG.MSG_1", {
                returnObjects: true,
            });
            let speechText2 = requestAttributes.t("Bienvenida_MSG.MSG_2", {
                returnObjects: true,
            });
            let speechText3 = requestAttributes.t("Bienvenida_MSG.MSG_3", {
                returnObjects: true,
            });
            let speechText4 = requestAttributes.t("Bienvenida_MSG.MSG_4", {
                returnObjects: true,
            });
            console.log("LaunchRequestHandler5");
            let mensajeSpeak =
                speechText0[Math.floor(Math.random() * speechText0.length)] +
                " " +
                speechText1[Math.floor(Math.random() * speechText1.length)] +
                " " +
                speechText2[Math.floor(Math.random() * speechText2.length)] +
                " " +
                speechText3[Math.floor(Math.random() * speechText3.length)] +
                " " +
                speechText4[Math.floor(Math.random() * speechText4.length)];

            mensajeSpeak = mensajeSpeak.replace("%s", name);
            console.log("LaunchRequestHandler6");
            return handlerInput.responseBuilder
                .speak(mensajeSpeak)
                .withShouldEndSession(false)
                .reprompt(
                    "¿Hay algo que quieras realizar?. puedes pedirme ayuda si no sabes qué decir."
                )
                .getResponse();
        } catch (error) {
            return (
                responseBuilder
                    .speak(
                        "MEdi-reco no tiene los permisos necesarios para el correcto funcionamiento, Necesito acceder a tu nombre para crear frases amigables. Por favor, Vé a la pantalla principal de tu aplicación Alexa y concede los permisos requeridos"
                    )
                    .withAskForPermissionsConsentCard(["alexa::profile:given_name:read"])
                    //.withAskForPermissionsConsentCard(['alexa::alerts:reminders:skill:readwrite'])
                    .withShouldEndSession(true)
                    .getResponse()
            );
        }
    },
};

const BienvenidaIntent = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'BienvenidaIntent';
    },
    handle(handlerInput) {
        return handlerInput.responseBuilder
            .speak("Bienvenido a Medi-reco! Me encargaré de acopañarte durante tu tratamiento médico.")
            .reprompt("Reprompt de bienvenida intent")
            .getResponse();
    }
};

const sugerenciaIntent = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'sugerenciaIntent';
    },
    handle(handlerInput) {
        //return LaunchRequestHandler.handle(handlerInput);
        const { attributesManager } = handlerInput;
        const requestAttributes = attributesManager.getRequestAttributes();
        const respuesta = "Puedes decir: quiero ingresar, modificar o eliminar un medicamento para empezar, o si lo prefieres tambien puedes pedirme una notica";
        return handlerInput.responseBuilder
            .speak(respuesta)
            .reprompt(requestAttributes.t('CONTINUE_MSG'))
            .getResponse();
    }
};

const MedicamentoRegistrarHorarioIntent = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'MedicamentoRegistrarHorarioIntent';
    },
    async handle(handlerInput) {
        console.log("estoy en: MedicamentoRegistrarHorarioIntent");
        const { attributesManager, serviceClientFactory } = handlerInput;
        const requestAttributes = attributesManager.getRequestAttributes();
        const intent = handlerInput.requestEnvelope.request.intent;
        const sessionAttributes = attributesManager.getSessionAttributes();
        const upsServiceClient = serviceClientFactory.getUpsServiceClient();
        const reminderApiClient = handlerInput.serviceClientFactory.getReminderManagementServiceClient(),
            { permissions } = handlerInput.requestEnvelope.context.System.user;
        try {
            if (!permissions) {
                return handlerInput.responseBuilder
                    .speak("Por favor ve a la aplicacion de alexa para conceder los permisos necesarios")
                    .withAskForPermissionsConsentCard(['alexa::alerts:reminders:skill:readwrite'])
                    .withShouldEndSession(true).getResponse();
            }
            //let existeDato = busqueda(sessionAttributes['recordatorio'], intent.slots.forma_medicamento.value);

            const causa = intent.slots.causal.value;
            const description = intent.slots.description.value;
            const hora = intent.slots.hora.value;
            const intervalo = intent.slots.intervalo.value;

            let respuesta = '';
            let horas = [];
            let recordatorio = '';
            let speechText1 = "";
            let speechText2 = "";
            let speechText3 = "";
            let mensaje = "";
            let inter = "";

            const name = await upsServiceClient.getProfileGivenName();
            //const name = sessionAttributes['name'] ? sessionAttributes['name'] : '';
            let ciclo = 0;
            for (let i = moment(hora, 'HH:mm'); i.isBefore(moment('23:59', 'HH:mm')); i.add(moment.duration(intervalo).asSeconds(), 'seconds')) {
                //for (let i = moment(hora, 'HH:mm'); ciclo !== 2; i.add(moment.duration(intervalo).asSeconds(), 'seconds')) {
                console.log(i);
                let regla = "FREQ=DAILY;BYHOUR=" + i.hours() + ";BYMINUTE=" + i.minutes() + ";BYSECOND=0;INTERVAL=1;";
                horas.push(regla);
                if (i === moment(hora, 'HH:mm')) {
                    ciclo = ciclo + 1;
                }
            }


            speechText3 = requestAttributes.t('RECORDATORIO_MSG.MEDICAMENTO_MSG', { returnObjects: true });
            recordatorio = speechText3[Math.floor(Math.random() * speechText3.length)];
            console.log("intervalo: " + intervalo);
            inter = intervalo.replace('PT', '').replace('H', ' horas ');
            if (inter[inter.length - 1] === "D") {//cuando el intervalo es Pnd donde n es el numero de dias, se cambia el mensaje por uno más natural
                inter.slice(1, -1);
                inter += " días."
            }
            recordatorio = recordatorio.replace('%s', name).replace('%s', causa).replace('%s', description).replace('%s', inter);
            let reminder = crearRecordario(horas, recordatorio);
            if (opcion !== "modificar") {
                let recordatorios = sessionAttributes['recordatorio'];

                if (recordatorios.length < 6) {
                    let existeDato = busqueda(recordatorios, description);
                    if (!existeDato) {
                        speechText1 = requestAttributes.t('REGISTER_MSG.MSG_1', { returnObjects: true });
                        speechText2 = requestAttributes.t('REGISTER_MSG.MSG_2', { returnObjects: true });
                        const id = sessionAttributes['recordatorio'].length + 1;

                        await reminderApiClient.createReminder(reminder).then((reminderResponse) => {
                            let recordatorioItem = {
                                "id": id,
                                "alertToken": reminderResponse.alertToken,
                                "causa": causa,
                                "description": description,
                                "hora": hora,
                                "intervalo": intervalo
                            };
                            sessionAttributes['recordatorio'].push(recordatorioItem);
                            mensaje = speechText2[Math.floor(Math.random() * speechText2.length)];
                            mensaje = mensaje.replace('%s', description).replace('%s', causa).replace('%s', inter);

                            respuesta += speechText1[Math.floor(Math.random() * speechText1.length)] + ' ' + mensaje;
                        }).catch((error) => {
                            console.log("Error en MedicamentoRegistrarHorarioIntent crear reminder: " + JSON.stringify(error));

                            if (error.statusCode === 400) {
                                respuesta = ("No ha sido registrado. El intervalo debe ser mayor a una hora, vuelve a intentarlo con un intervalo diferente")
                            }
                            else {
                                respuesta = requestAttributes.t('ERROR_MSG');
                            }
                        });
                    } else {
                        respuesta = "Los siento " + name + ", ya tengo un medicamento guardado con ese color, intenta guardarlo con un color distinto";
                    }
                } else {
                    respuesta = "Los siento " + name + ", has superado la cantidad de medicamento que te puedo recordar, intenta modificar o eliminar algunos de los que tienes registrado";
                }
            } else {
                speechText1 = requestAttributes.t('ACTUALIZAR_MSG.MSG_1', { returnObjects: true });
                speechText2 = requestAttributes.t('ACTUALIZAR_MSG.MSG_2', { returnObjects: true });
                let alertToken = miRecordatorio.alertToken;

                await reminderApiClient.updateReminder(alertToken, reminder).then((reminderRequest) => {
                    miRecordatorio.alertToken = reminderRequest.alertToken;
                    miRecordatorio.causa = causa;
                    miRecordatorio.description = description;
                    miRecordatorio.hora = hora;
                    miRecordatorio.intervalo = intervalo;

                    sessionAttributes['recordatorio'].splice(miRecordatorio.id - 1, 1, miRecordatorio);
                    miRecordatorio = null;
                    respuesta += speechText1[Math.floor(Math.random() * speechText1.length)] + ' ' + mensaje;
                }).catch((error) => {
                    console.log("Error en MedicamentoRegistrarHorarioIntent crear reminder: " + JSON.stringify(error));

                    if (error.statusCode === 400) {
                        respuesta = ("No ha sido registrado. El intervalo debe ser mayor a una hora, vuelve a intentarlo con un intervalo diferente")
                    }
                    else {
                        respuesta = requestAttributes.t('ERROR_MSG');
                    }
                });
                opcion = "";
            }
            respuesta += "... ¿Que quieres hacer ahora?";
            return handlerInput.responseBuilder
                .speak(respuesta)
                .reprompt(requestAttributes.t('CONTINUE_MSG'))
                .withShouldEndSession(false)
                .getResponse();
        }
        catch (error) {
            const speechText = requestAttributes.t('ERROR_MSG');
            console.log("Error en el catch de MedicamentoRegistrarHorarioIntent: " + error);
            return handlerInput.responseBuilder
                .speak(speechText)
                .withAskForPermissionsConsentCard(['alexa::alerts:reminders:skill:readwrite'])
                .withShouldEndSession(true);
        }
    }
};

const ListarRecordatorioIntent = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'ListarRecordatorioIntent';
    },
    handle(handlerInput) {
        console.log("estoy en ListarRecordatorioIntent");
        const { attributesManager } = handlerInput;
        const requestAttributes = attributesManager.getRequestAttributes();
        const sessionAttributes = attributesManager.getSessionAttributes();
        const shouldEndSession = false;

        let speechText = '';
        let aux = '';
        let recordatorios = sessionAttributes['recordatorio'];
        let cantidad = recordatorios.length;

        if (cantidad < 1) {
            speechText = requestAttributes.t('MSG');
        } else {
            speechText += 'Tienes ' + cantidad + ' recodatorios registrados. ';
            speechText += 'Son los siguientes: ';
            recordatorios.forEach((item, i) => {
                const { causa, description, hora, intervalo } = item;
                let inter = intervalo.replace('PT', '').replace('H', ' horas ');
                aux = 'Medicamento ' + ' ' + item.id + ': %s, para %s a las %s cada %s todos los dias.        ';
                speechText += aux.replace('%s', description).replace('%s', causa).replace('%s', hora).replace('%s', inter);
            });
            speechText += 'Han sido todos. Si quieres agregar uno nuevo o eliminar uno ya existente, solo debes decirmelo.';
        }

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(requestAttributes.t('CONTINUE_MSG'))
            .withShouldEndSession(shouldEndSession)
            .getResponse();
        //.LaunchRequestHandler.handle(handlerInput);
    }
};

const MedicamentoModificarHorarioIntent = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'MedicamentoModificarHorarioIntent';
    },
    handle(handlerInput) {
        const { attributesManager } = handlerInput;
        const requestAttributes = attributesManager.getRequestAttributes();
        const intent = handlerInput.requestEnvelope.request.intent;
        const sessionAttributes = attributesManager.getSessionAttributes();
        const name = sessionAttributes['name'] ? sessionAttributes['name'] : '';
        const description = intent.slots.description.value;
        let respuesta ='';

        var recordatorios = sessionAttributes['recordatorio'];
        miRecordatorio = busqueda(recordatorios, description);

        if (miRecordatorio) {
            let reminder = '';
            let speechText3 = ' %s de color %s que tomas a las %s cada %s';

            reminder = speechText3.replace('%s', miRecordatorio.causa)
                .replace('%s', miRecordatorio.description)
                .replace('%s', miRecordatorio.hora)
                .replace('%s', miRecordatorio.intervalo.replace('PT', '').replace('H', ' horas '));

            handlerInput.responseBuilder.addDelegateDirective(
                {
                    name: 'confirmarMedicamentoIntent',
                    confirmationStatus: 'NONE',
                    slots: {
                        recordatorio:
                        {
                            name: 'recordatorio',
                            value: reminder,
                            confirmationStatus: 'NONE'
                        }
                    }
                });
        } else {
            respuesta += 'Lo siento, pero no he encontrado un medicamento de color: ' + description + '. Qué deseas hacer ahora?';
        }
        return handlerInput.responseBuilder
                .speak(respuesta)
            .reprompt(requestAttributes.t('CONTINUE_MSG'))
            .withShouldEndSession(false)
            .getResponse();
    }
};

const EliminarMedicamentoIntent = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'EliminarMedicamentoIntent';
    },
    handle(handlerInput) {
        const { attributesManager } = handlerInput;
        const requestAttributes = attributesManager.getRequestAttributes();
        const intent = handlerInput.requestEnvelope.request.intent;
        const sessionAttributes = attributesManager.getSessionAttributes();
        const name = sessionAttributes['name'] ? sessionAttributes['name'] : '';
        const description = intent.slots.description.value;

        var recordatorios = sessionAttributes['recordatorio'];
        miRecordatorio = busqueda(recordatorios, description);

        let respuesta = '';
        if (miRecordatorio) {
            let reminder = '';
            let speechText3 = ' %s de %s %s a las %s cada %s todos los dias.';

            sessionAttributes['recordatorio'].splice(miRecordatorio.id - 1, 1);
            respuesta = 'El medicamento de color' + description + " ha sido eliminado correctamente";
        } else {
            respuesta = 'Lo siento no pude encontrar el medicamento: ' + description;
        }
            respuesta += "... ¿Que quieres hacer ahora?";
        return handlerInput.responseBuilder
                .speak(respuesta)
            .reprompt(requestAttributes.t('CONTINUE_MSG'))
            .withShouldEndSession(false)
            .getResponse();
    }
};

const MedicamentoBuscarHorarioIntent = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'MedicamentoBuscarHorarioIntent';
    },
    handle(handlerInput) {
        const { attributesManager } = handlerInput;
        const requestAttributes = attributesManager.getRequestAttributes();
        const intent = handlerInput.requestEnvelope.request.intent;
        const sessionAttributes = attributesManager.getSessionAttributes();
        const name = sessionAttributes['name'] ? sessionAttributes['name'] : '';
        let description = "";
        description = intent.slots.descripcion.value;

        var recordatorios = sessionAttributes['recordatorio'];
        miRecordatorio = busqueda(recordatorios, description);

        if (miRecordatorio) {
            let reminder = '';
            let speechText3 = 'El medicamento %s para %s a las %s cada %s todos los dias.';

            reminder = speechText3.replace('%s', description)
                .replace('%s', miRecordatorio.causa)
                .replace('%s', miRecordatorio.hora)
                .replace('%s', miRecordatorio.intervalo.replace('PT', '').replace('H', ' horas '));
                reminder += "Dime que quieres hacer ahora? solo tienes que decirmelo."
            handlerInput.responseBuilder
                .speak(reminder)
                .reprompt(requestAttributes.t('CONTINUE_MSG'))
            .withShouldEndSession(false)
            .getResponse();

        } else {
            handlerInput.responseBuilder
                .speak('Lo siento no pude encontrar el medicamento: ' + description + 'asegurate de que el color es el correcto, si lo olvidaste pideme que liste todos los medicamentos');
        }

        return handlerInput.responseBuilder
            .reprompt(requestAttributes.t('CONTINUE_MSG')).getResponse();
    }
};

const confirmarMedicamentoIntent = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'confirmarMedicamentoIntent';
    },
    handle(handlerInput) {
        const intent = handlerInput.requestEnvelope.request.intent;
        if (intent.confirmationStatus === 'CONFIRMED') {
            if (miRecordatorio) {
                opcion = "modificar";
                handlerInput.responseBuilder.addDelegateDirective(
                    {
                        name: 'MedicamentoRegistrarHorarioIntent',
                        confirmationStatus: 'NONE',
                        slots: {}
                    });
            }
        } else {
            opcion = "";
            handlerInput.responseBuilder
                .speak('Vale. Proceso cancelado. Recuerda que puedo guardar tus medicamentos si me dices el horario.')
                .reprompt('Que quieres hacer ahora, recuerda que puedes pedirme ayuda si lo deseas.')
                .getResponse();
        }
        return handlerInput.responseBuilder.getResponse();
    }
};

const NoticiaIntent = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'NoticiaIntent';
    },
    handle(handlerInput) {
        const { attributesManager } = handlerInput;
        const requestAttributes = attributesManager.getRequestAttributes();
        const sessionAttributes = attributesManager.getSessionAttributes();

        let speechText1 = requestAttributes.t('Noticia_MSG.MSG_1', { returnObjects: true });
        let speechText2 = requestAttributes.t('Noticia_MSG.MSG_2', { returnObjects: true });
        let speechText3 = requestAttributes.t('Noticia_MSG.MSG_3', { returnObjects: true });
        let respuesta = //speechText1[Math.floor(Math.random() * speechText1.length)] + ' ' +
            speechText2[Math.floor(Math.random() * speechText2.length)] + ' '
            //+ speechText3[Math.floor(Math.random() * speechText3.length)]
            ;

        return handlerInput.responseBuilder
            .speak(respuesta)
            .reprompt(requestAttributes.t('CONTINUE_MSG'))
            .withShouldEndSession(false)
            .getResponse();
    }
};

const NoticiaCualquieraIntent = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'NoticiaCualquieraIntent';
    },
    async handle(handlerInput) {
        const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
        const intent = handlerInput.requestEnvelope.request.intent;

        let urls = requestAttributes.t('Noticia_URL', { returnObjects: true });
        let speakOutput = "Encontre esta noticia que te puede interesar. ";
        let parser = new Parser();

        let url = urls[Math.floor(Math.random() * (urls.length - 1))];
        let feed = await parser.parseURL(url);
        let noticia = feed.items[Math.floor(Math.random() * (feed.items.length))];
        speakOutput += noticia.title + ". " + noticia.contentSnippet + " puedes escuchar mas noticias si dices algo como: quiero una nueva noticia";

        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(requestAttributes.t('CONTINUE_MSG'))
            .withShouldEndSession(false)
            .getResponse();
    }
};

const NoticiaCienciaIntent = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'NoticiaCienciaIntent';
    },
    async handle(handlerInput) {
        const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
        const intent = handlerInput.requestEnvelope.request.intent;

        let speechText1 = requestAttributes.t('Noticia_tecno', { returnObjects: true });
        const url = speechText1[Math.floor(Math.random() * (speechText1.length - 1))];

        let parser = new Parser();
        let speakOutput = "Encontre esta noticia sobre ciencia y tecnología . ";

        let feed = await parser.parseURL(url);
        var noticia = feed.items[Math.floor(Math.random() * (feed.items.length))];
        speakOutput += noticia.title;
        speakOutput += ". " + noticia.contentSnippet;
        speakOutput += ". Te ha gustado esta noticia? puedo contarte más. Dime, ¿qué quieres hacer ahora?"
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(requestAttributes.t('CONTINUE_MSG'))
            .withShouldEndSession(false)
            .getResponse();
    }
};

const NoticiaDeporteIntent = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'NoticiaDeporteIntent';
    },
    async handle(handlerInput) {
        const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
        const intent = handlerInput.requestEnvelope.request.intent;

        let speechText1 = requestAttributes.t('Noticia_deportes', { returnObjects: true });
        const url = speechText1[Math.floor(Math.random() * (speechText1.length - 1))];

        let parser = new Parser();
        let speakOutput = "Encontre esta noticia de deportes. ";

        let feed = await parser.parseURL(url);
        var noticia = feed.items[Math.floor(Math.random() * (feed.items.length))];
        speakOutput += noticia.title;
        speakOutput += ". " + noticia.contentSnippet;
        speakOutput += ". Te ha gustado esta noticia? puedo contarte más. Dime, ¿qué quieres hacer ahora?"
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(requestAttributes.t('CONTINUE_MSG'))
            .withShouldEndSession(false)
            .getResponse();
    }
};

const NoticiaFarandulaIntent = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'NoticiaFarandulaIntent';
    },
    async handle(handlerInput) {
        const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
        const intent = handlerInput.requestEnvelope.request.intent;

        let speechText1 = requestAttributes.t('Noticia_farandula', { returnObjects: true });
        const url = speechText1[Math.floor(Math.random() * (speechText1.length - 1))];

        let parser = new Parser();
        let speakOutput = "Encontre esta noticia sobre farandula.";

        let feed = await parser.parseURL(url);
        var noticia = feed.items[Math.floor(Math.random() * (feed.items.length))];
        speakOutput += noticia.title;
        speakOutput += ". " + noticia.contentSnippet;
        speakOutput += ". Te ha gustado esta noticia? puedo contarte más. Dime, ¿qué quieres hacer ahora?"
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(requestAttributes.t('CONTINUE_MSG'))
            .withShouldEndSession(false)
            .getResponse();
    }
};

const NoticiaJudicialIntent = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'NoticiaJudicialIntent';
    },
    async handle(handlerInput) {
        const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
        const intent = handlerInput.requestEnvelope.request.intent;

        let speechText1 = requestAttributes.t('Noticia_judicial', { returnObjects: true });
        const url = speechText1[Math.floor(Math.random() * (speechText1.length - 1))];
        let speakOutput = "Encontre esta noticia judicial.";

        let parser = new Parser();
        let feed = await parser.parseURL(url);
        var noticia = feed.items[Math.floor(Math.random() * (feed.items.length))];
        speakOutput += noticia.title;
        speakOutput += ". " + noticia.contentSnippet;
        speakOutput += ". Te ha gustado esta noticia? puedo contarte más. Dime, ¿qué quieres hacer ahora?"
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(requestAttributes.t('CONTINUE_MSG'))
            .withShouldEndSession(false)
            .getResponse();
    }
};

const NoticiaEconomiaIntent = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'NoticiaEconomiaIntent';
    },
    async handle(handlerInput) {
        const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
        const intent = handlerInput.requestEnvelope.request.intent;

        let speechText1 = requestAttributes.t('Noticia_economia', { returnObjects: true });
        console.log("noticias economia: " + speechText1.length);
        let nu = Math.floor(Math.random() * (speechText1.length - 1));
        console.log(nu)
        const url = speechText1[nu];

        let parser = new Parser();
        let speakOutput = "Encontre esta noticia sobre economia. ";
        let feed = await parser.parseURL(url);
        var noticia = feed.items[Math.floor(Math.random() * (feed.items.length))];
        speakOutput += noticia.title;
        speakOutput += ". " + noticia.contentSnippet;
        speakOutput += ". Te ha gustado esta noticia? puedo contarte más. Dime, ¿qué quieres hacer ahora?"
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(requestAttributes.t('CONTINUE_MSG'))
            .withShouldEndSession(false)
            .getResponse();
    }
};

const NoticiaPoliticaIntent = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'NoticiaiPoliticaIntent';
    },
    async handle(handlerInput) {
        const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
        const intent = handlerInput.requestEnvelope.request.intent;

        let speechText1 = requestAttributes.t('Noticia_politica', { returnObjects: true });
        const url = speechText1[Math.floor(Math.random() * (speechText1.length - 1))];

        let parser = new Parser();
        let speakOutput = "Encontre esta noticia sobre política. ";
        let feed = await parser.parseURL(url);
        var noticia = feed.items[Math.floor(Math.random() * (feed.items.length))];
        speakOutput += noticia.title;
        speakOutput += ". " + noticia.contentSnippet;
        speakOutput += ". Te ha gustado esta noticia? puedo contarte más. Dime, ¿qué quieres hacer ahora?"
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(requestAttributes.t('CONTINUE_MSG'))
            .withShouldEndSession(false)
            .getResponse();
    }
};

const NoticiaNacionalIntent = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'NoticiaCiudadesIntent';
    },
    async handle(handlerInput) {
        const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
        const intent = handlerInput.requestEnvelope.request.intent;

        let speechText1 = requestAttributes.t('Noticia_nacional', { returnObjects: true });
        const url = speechText1[Math.floor(Math.random() * (speechText1.length - 1))];

        let parser = new Parser();
        let speakOutput = "Encontre esta noticia sobre lo que está pasando en Colombia. ";
        let feed = await parser.parseURL(url);
        var noticia = feed.items[Math.floor(Math.random() * (feed.items.length))];
        speakOutput += noticia.title;
        speakOutput += ". " + noticia.contentSnippet;
        speakOutput += ". Te ha gustado esta noticia? puedo contarte más. Dime, ¿qué quieres hacer ahora?"
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(requestAttributes.t('CONTINUE_MSG'))
            .withShouldEndSession(false)
            .getResponse();
    }
};

const NoticiaInteracionalIntent = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'NoticiaInternacionalIntent';
    },
    async handle(handlerInput) {
        const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
        const intent = handlerInput.requestEnvelope.request.intent;

        let speechText1 = requestAttributes.t('Noticia_internacional', { returnObjects: true });
        const url = speechText1[Math.floor(Math.random() * (speechText1.length - 1))];

        let parser = new Parser();
        let speakOutput = "Encontre esta noticia sobre lo que está pasando en el mundo. ";
        let feed = await parser.parseURL(url);
        var noticia = feed.items[Math.floor(Math.random() * (feed.items.length))];
        speakOutput += noticia.title;
        speakOutput += ". " + noticia.contentSnippet;
        speakOutput += ". Te ha gustado esta noticia? puedo contarte más. Dime, ¿qué quieres hacer ahora?"
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(requestAttributes.t('CONTINUE_MSG'))
            .withShouldEndSession(false)
            .getResponse();
    }
};

const NoticiaSaludIntent = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'NoticiaSaludIntent';
    },
    async handle(handlerInput) {
        const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
        const intent = handlerInput.requestEnvelope.request.intent;

        let speechText1 = requestAttributes.t('Noticia_salud', { returnObjects: true });
        const url = speechText1[Math.floor(Math.random() * (speechText1.length - 1))];

        let parser = new Parser();
        let speakOutput = "Encontre esta noticia saludable y de novedades. ";
        let feed = await parser.parseURL(url);
        var noticia = feed.items[Math.floor(Math.random() * (feed.items.length))];
        speakOutput += noticia.title;
        speakOutput += ". " + noticia.contentSnippet;
        speakOutput += ". Te ha gustado esta noticia? puedo contarte más. Dime, ¿qué quieres hacer ahora?"
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(requestAttributes.t('CONTINUE_MSG'))
            .withShouldEndSession(false)
            .getResponse();
    }
};

const NoticiaTodasIntent = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'NoticiaTodasIntent';
    },
    async handle(handlerInput) {
        const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
        const intent = handlerInput.requestEnvelope.request.intent;

        let speechText1 = requestAttributes.t('Noticia_URL', { returnObjects: true });
        var speakOutput = "Este es un repaso de las noticias. ";
        var parser = new Parser();
        try {
            for (let j = 0; j < 3; j++) {
                let url = speechText1[j];
                let feed = await parser.parseURL(url);
                let noticia = feed.items[Math.floor(Math.random() * (feed.items.length - 1))];
                speakOutput += noticia.title;
                speakOutput += ". " + noticia.contentSnippet;
            }
        } catch (error) {
            console.log("Error en NoticiaTodasIntent: " + JSON.stringify(error));
            const speechText = requestAttributes.t('ERROR_MSG');
            return handlerInput.responseBuilder
                .speak(speechText)
                .withShouldEndSession(true)
                .getResponse();
        }
        speakOutput += ".  ¿Qué quieres hacer ahora?"
        return handlerInput.responseBuilder
            .speak(speakOutput)
            .reprompt(requestAttributes.t('CONTINUE_MSG'))
            .withShouldEndSession(false)
            .getResponse();
    }
};

const HelpIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.HelpIntent';
    },
    handle(handlerInput) {
        const { attributesManager } = handlerInput;
        const requestAttributes = attributesManager.getRequestAttributes();
        const speechText = requestAttributes.t('HELP_MSG');
        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt("Si no ha sido claro, puedes pedirme que te dé una guia completa")
            .withShouldEndSession(false)
            .getResponse();
    }
};

const guiaSimpleIntent = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'guiaSimpleIntent';
    },
    handle(handlerInput) {
        const { attributesManager } = handlerInput;
        const requestAttributes = attributesManager.getRequestAttributes();
        let mensajes = requestAttributes.t('HELP_SIMPLE_MSG', { returnObjects: true });
        const speechText = mensajes[Math.floor(Math.random() * mensajes.length)];
        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt("Si no ha sido claro, puedes pedirme que te dé una guia completa")
            .withShouldEndSession(false)
            .getResponse();
    }
};

const GuiaCompletaIntent = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'GuiaCompletaIntent';
    },
    handle(handlerInput) {
        const { attributesManager } = handlerInput;
        const requestAttributes = attributesManager.getRequestAttributes();
        const speechText = requestAttributes.t('HELP_COMPLETE_MSG');

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt("No he entendido lo que haz dicho he entendido, vuelve a pedirme lo que quieres hacer, por ejemplo, registrar un medicamento o escuchar una nueva noticia.")
            .withShouldEndSession(false)
            .getResponse();
    }
};

const CancelAndStopIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.CancelIntent'
                || handlerInput.requestEnvelope.request.intent.name === 'AMAZON.StopIntent');
    },
    handle(handlerInput) {
        const { attributesManager } = handlerInput;
        const requestAttributes = attributesManager.getRequestAttributes();
        const speechText = requestAttributes.t('GOODBYE_MSG');

        return handlerInput.responseBuilder
            .speak(speechText)
            .getResponse();
    }
};

const FallbackIntentHandler =
{
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.FallbackIntent';
    },
    handle(handlerInput) {
        const { attributesManager } = handlerInput;
        const requestAttributes = attributesManager.getRequestAttributes();
        const speechText = requestAttributes.t('GOODBYE_MSG');

        return handlerInput.responseBuilder
            .speak('estoy en el FallbackIntentHandler')
            .getResponse();
    }
};

const SessionEndedRequestHandler =
{
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
    },
    handle(handlerInput) {
        // Any cleanup logic goes here.
        return handlerInput.responseBuilder.getResponse();
    }
};

// The intent reflector is used for interaction model testing and debugging.
// It will simply repeat the intent the user said. You can create custom handlers
// for your intents by defining them above, then also adding them to the request
// handler chain below.
const IntentReflectorHandler =
{
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest';
    },
    handle(handlerInput) {
        const { attributesManager } = handlerInput;
        const requestAttributes = attributesManager.getRequestAttributes();
        const intentName = handlerInput.requestEnvelope.request.intent.name;
        const speechText = requestAttributes.t('REFLECTOR_MSG', intentName);

        return handlerInput.responseBuilder
            .speak(speechText)
            //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
            .getResponse();
    }
};

// Generic error handling to capture any syntax or routing errors. If you receive an error
// stating the request handler chain is not found, you have not implemented a handler for
// the intent being invoked or included it in the skill builder below.
const ErrorHandler =
{
    canHandle() {
        return true;
    },
    handle(handlerInput, error) {
        const { attributesManager } = handlerInput;
        const requestAttributes = attributesManager.getRequestAttributes();
        const speechText = requestAttributes.t('ERROR_MSG ' + error.message);

        console.log(`~~~~ Ha ocurrido un error: ${error.message}`);

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .getResponse();
    }
};


// This request interceptor will log all incoming requests to this lambda
const LoggingRequestInterceptor = {
    process(handlerInput) {
        console.log(`Incoming request: ${JSON.stringify(handlerInput.requestEnvelope.request)}`);
    }
};

// This response interceptor will log all outgoing responses of this lambda
const LoggingResponseInterceptor =
{
    process(handlerInput, response) {
        console.log(`Outgoing response: ${JSON.stringify(response)}`);
    }
};

// This request interceptor will bind a translation function 't' to the requestAttributes.
const LocalizationRequestInterceptor =
{
    process(handlerInput) {
        const localizationClient = i18n.use(sprintf).init({
            lng: handlerInput.requestEnvelope.request.locale,
            overloadTranslationOptionHandler: sprintf.overloadTranslationOptionHandler,
            resources: languageStrings,
            returnObjects: true
        });
        const attributes = handlerInput.attributesManager.getRequestAttributes();
        attributes.t = function (...args) {
            return localizationClient.t(...args);
        }
    }
};

const LoadAttributesRequestInterceptor =
{
    async process(handlerInput) {
        if (handlerInput.requestEnvelope.session['new']) { //is this a new session?
            const { attributesManager } = handlerInput;
            const persistentAttributes = await attributesManager.getPersistentAttributes() || {};
            //copy persistent attribute to session attributes
            handlerInput.attributesManager.setSessionAttributes(persistentAttributes);
        }
    }
};

const SaveAttributesResponseInterceptor = {
    async process(handlerInput, response) {
        const { attributesManager } = handlerInput;
        const sessionAttributes = attributesManager.getSessionAttributes();
        console.log("estoy en SaveAttributesResponseInterceptor");
        const shouldEndSession = (typeof response.shouldEndSession === "undefined" ? true : response.shouldEndSession);//is this a session end?
        if (shouldEndSession || handlerInput.requestEnvelope.request.type === 'SessionEndedRequest') { // skill was stopped or timed out            
            attributesManager.setPersistentAttributes(sessionAttributes);
            await attributesManager.savePersistentAttributes();
        }
    }
};
function getPersistenceAdapter() {
    // This function is an indirect way to detect if this is part of an Alexa-Hosted skill
    function isAlexaHosted() {
        return process.env.S3_PERSISTENCE_BUCKET ? true : false;
    }
    const tableName = 'happy_birthday_table';
    if (isAlexaHosted()) {
        const { S3PersistenceAdapter } = require('ask-sdk-s3-persistence-adapter');
        return new S3PersistenceAdapter({
            bucketName: process.env.S3_PERSISTENCE_BUCKET
        });
    } else {
        // IMPORTANT: don't forget to give DynamoDB access to the role you're to run this lambda (IAM)
        const { DynamoDbPersistenceAdapter } = require('ask-sdk-dynamodb-persistence-adapter');
        return new DynamoDbPersistenceAdapter({
            tableName: tableName,
            createTable: true
        });
    }
}

let crearRecordario = (horas, mensaje) => {
    const currentDateTime = moment().tz('America/Bogota'),
        reminderRequest =
        {
            requestTime: currentDateTime.format('YYYY-MM-DDTHH:mm:ss'),
            trigger:
            {
                type: 'SCHEDULED_ABSOLUTE',
                scheduledTime: currentDateTime.format('YYYY-MM-DDTHH:mm:ss'),
                timeZoneId: 'America/Bogota',
                recurrence:
                {
                    freq: 'DAILY',
                    recurrenceRules: horas
                }
            },
            alertInfo:
            {
                spokenInfo:
                {
                    content:
                        [{
                            locale: "es-CO",
                            text: mensaje,
                        }],
                },
            },
            pushNotification:
            {
                "status": "ENABLED",
            }
        }
    return reminderRequest;
}

let crearRecordarioUnico = (mensaje) => {
    let today = new Date();
    let hora = today.getHours();
    console.log("crearRecordarioUnico0: " + hora);
    hora += Math.floor(Math.random() * 12);
    console.log("crearRecordarioUnico000: " + hora);
    if (hora < 12 || hora > 23) {
        hora = 12 + Math.floor(Math.random() * 12);
        if (today.getHours() > hora) { // si es más temprano de la hora actual
            today.setDate(today.getDate() + 1)
        }
    }
    console.log("crearRecordarioUnico1: " + hora);
    //let fecha = today.toLocaleDateString('it-IT') + "T" + today.toLocaleTimeString('it-IT') + ".000";
    let fecha = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate() + "T" +
        (hora - 5) + ":" + today.getMinutes() + ":" + today.getSeconds() + ".000";
    /*if (today.getHours()>20){
        fechaRecordatorio += 60000
    }*/
    console.log("crearRecordarioUnico2: " + fecha);
    const currentDateTime = moment().tz('America/Bogota'),
        reminderRequest =
        {
            requestTime: currentDateTime.format('YYYY-MM-DDTHH:mm:ss'),
            trigger:
            {
                type: 'SCHEDULED_ABSOLUTE',
                //type: 'SCHEDULED_RELATIVE',
                timeZoneId: 'America/Bogota',
                //offsetInSeconds :3600
                //scheduledTime : "2021-12-2T19:00:00.000"
                scheduledTime: fecha
            },
            alertInfo:
            {
                spokenInfo:
                {
                    content:
                        [{
                            locale: "es-CO",
                            text: mensaje,
                        }],
                },
            },
            pushNotification:
            {
                "status": "ENABLED",
            }
        }
    return reminderRequest;
}

/**
 * This handler acts as the entry point for your skill, routing all request and response
 * payloads to the handlers above. Make sure any new handlers or interceptors you've
 * defined are included below. The order matters - they're processed top to bottom 
 * */
exports.handler = Alexa.SkillBuilders.custom()
    .addRequestHandlers(
        LaunchRequestHandler,
        BienvenidaIntent,
        NoticiaIntent,
        NoticiaCienciaIntent,
        NoticiaCualquieraIntent,
        NoticiaDeporteIntent,
        NoticiaFarandulaIntent,
        NoticiaJudicialIntent,
        NoticiaPoliticaIntent,
        NoticiaEconomiaIntent,
        NoticiaNacionalIntent,
        NoticiaInteracionalIntent,
        NoticiaSaludIntent,
        NoticiaTodasIntent,
        MedicamentoRegistrarHorarioIntent,
        MedicamentoModificarHorarioIntent,
        MedicamentoBuscarHorarioIntent,
        ListarRecordatorioIntent,
        EliminarMedicamentoIntent,
        HelpIntentHandler,
        CancelAndStopIntentHandler,
        FallbackIntentHandler,
        SessionEndedRequestHandler,
        confirmarMedicamentoIntent,
        activarPermisosIntent,
        guiaSimpleIntent,
        GuiaCompletaIntent,
        sugerenciaIntent,
        IntentReflectorHandler)
    .addErrorHandlers(ErrorHandler)
    .addRequestInterceptors(
        LocalizationRequestInterceptor,
        LoggingRequestInterceptor,
        LoadAttributesRequestInterceptor)
    .addResponseInterceptors(
        LoggingResponseInterceptor,
        SaveAttributesResponseInterceptor)
    .withApiClient(new Alexa.DefaultApiClient())
    .withPersistenceAdapter(persistenceAdapter).lambda();
